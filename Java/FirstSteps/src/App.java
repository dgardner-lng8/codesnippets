import java.util.TreeSet;
import java.util.HashSet;
import java.util.Set;

public class App {
    public static void main(String[] args) throws Exception {
        int []testIds = {4,3,2,1,5,6,7,8,9,120482};
        int missingId = findMIssingServerId(testIds);

        System.out.println("Missing server ID: " + missingId);
    }

    public static int findMIssingServerId(int [] serverIDs){
        int length = serverIDs.length;
        if(length == 0 || (length == 1 && serverIDs[0] != 1)){
            return 1;
        }
        else if(length == 1){
            return 2;
        }
        else if(length == 2){
            int lower = serverIDs[0] > serverIDs[1] ? serverIDs[0] : serverIDs[1];
            int diff = Math.abs(serverIDs[0] - serverIDs[1]);
            int alternate = diff == 1 ? lower + 2 : lower + 1;

            return lower > 1 ? 1 : alternate;
        }

        Set<Integer> seen = new HashSet<>();
        Set<Integer> missingItems = new TreeSet<>();
        
        for(int x = 0, j = length - 1; x < j ; x++, j--){
            int leftA = serverIDs[x] ; 
            int leftB = serverIDs[x+1];
            int rightA = serverIDs[j] ; 
            int rightB = serverIDs[j-1];

            compareAndUpdateMissingItems(missingItems,leftA, leftB, seen);
            compareAndUpdateMissingItems(missingItems,rightA, rightB, seen);
        }

        if(!seen.contains(1)){
            return 1;
        }
        
        return missingItems.iterator().next();
        

    }

    private static void compareAndUpdateMissingItems(Set<Integer> missingItems, int aValue, int bValue, Set<Integer> seenItems) {
        int distance = Math.abs(aValue - bValue);
        int lower = aValue < bValue ? aValue : bValue;

        seenItems.add(aValue);
        seenItems.add(bValue);

        missingItems.removeIf(item -> {
            return item == aValue || item == bValue;
        });

        if(distance > 1){
            for( ; distance > 0 ; distance--){
                lower += 1;
                if(!seenItems.contains(lower) && !missingItems.contains(lower)){
                    missingItems.add(lower);
                }
                else if(seenItems.contains(lower) && missingItems.contains(lower)){
                    missingItems.remove(lower);
                }
            }
        }
    }
}
