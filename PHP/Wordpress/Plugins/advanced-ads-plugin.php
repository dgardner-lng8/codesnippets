// shortcode to display the advanced ads products
//[advertise-with-us show="list"]
function advertise_with_us( $atts ){
  $taxonomy = 'product_cat';
  $html = '<div class="product_list__by__category">';
  $cat = '';
  //$html = '<ul class="accordion__container">';
  if(is_woocommerce_activated()){
     // Get subcategories of the current category
     
     $terms    = get_terms([
      'taxonomy'    => $taxonomy,
      'hide_empty'  => true,
      'parent' => $atts['parent_cat']
    ]); 
    $count = 0;
    // Loop through product subcategories WP_Term Objects
    foreach ( $terms as $term ) {

      $html .= ( $count > 0 ? '<div class="separator"></div>': '')
              .'<h2 class="cat_header_cust">'. $term->name .'</h2>'
              .'<ul class="accordion__container">';

              // loop through the products for that category
              $args = array(
                'post_type' => 'product',
                'posts_per_page' => 12,
                'tax_query' => array(
                  'taxonomy' => 'product_cat',
                  'field'    => 'term_id',
                  'terms'     =>  $terms->term_id, 
                  'operator'  => 'IN'
                  )
              );
              $loop = new WP_Query( $args );
              $posts = $loop->posts;

              foreach($posts as $post){
                $has_thumbnail = has_post_thumbnail($post) ;
                $html .= '<li class="accordion__item">'
                        . '<div class="a-i-header">'
                            . ($has_thumbnail ? '<span class="header--icon" style="width:200px">'.get_the_post_thumbnail($post).'</span>' : '')
                            . '<span class="header--content '.($has_thumbnail ? 'has--icon': '').'">'
                                .'<h3>'.get_the_title($post).'</h3>'
                                .'<div class="d-md-none">'.apply_filters( 'woocommerce_short_description', $post->post_excerpt ).'</div>'
                            .'</span>'
                            .'<span class="header-action-icon"><i class="fas fa-2x fa-angle-down"></i></span>'
                        .'</div>'
                        .'<div class="a-i-content">'
                            .'<div class="btn-container"><a href="'.get_permalink($post).'" class="button">Add To Order</a></div>'
                            .apply_filters('the_content', $post->post_content)
                          .'</div>'
                        .'</li>';

                        $count = $count + 1;
              }

      $html .= '</ul>';
    } 
		wp_reset_postdata();
  }
  return $html.'</div>';
}
add_shortcode( 'advertise-with-us', 'advertise_with_us' );