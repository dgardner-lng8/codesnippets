function advertise_with_us_categories( $atts ){
  $html ='<div class="product_cats_with_desc_and_btn">';
  if(is_woocommerce_activated()){
      $taxonomy = 'product_cat';
      // Get subcategories of the current category
        $terms    = get_terms([
          'taxonomy'    => $taxonomy,
          'hide_empty'  => true,
          'parent' => $atts['parent_cat']
      ]);

      // Loop through product subcategories WP_Term Objects
      foreach ( $terms as $term ) {
        $term_link = get_term_link( $term, $taxonomy );

        $html .= '<div class="'. $term->slug .' cat_segment">'
                  .'<h2>'. $term->name .'</h2>'
                  .'<div class="cat_'.$term->slug.'_description cat_description">'.$term->description.'</div>'
                    .'<div class="centered mt-3"><a href="'. $term_link .'" class="button">Order Advertising Spots</a></div>'
                  .'</div>';
      }
  }
  wp_reset_postdata();
  return $html.'</div>';
}
add_shortcode( 'advertise-with-us-categories', 'advertise_with_us_categories' );